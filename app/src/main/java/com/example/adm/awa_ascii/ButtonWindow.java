package com.example.adm.awa_ascii;

import android.graphics.Color;

public class ButtonWindow extends AsciiWindow {
	final private char[] text;
	Runnable onClick;
	public ButtonWindow(AsciiView view, int x, int y, int w, int h, String text, Runnable onClick) {
		super(view, x, y, w, h);
		int placeSize = w-2;
		this.text = new char[text.length()];
		for (int i = 0; i < text.length() && i < placeSize; i++)
			this.text[i] = text.charAt(i);
		this.onClick = onClick;
	}
	ButtonWindow(AsciiView view, int x, int y, int w, int h, char[] text, Runnable onClick) {
		super(view, x, y, w, h);
		this.text = text;
		this.onClick = onClick;
	}

	@Override
	void update() {
		if (getWidth() < 3 || getHeight() < 3) return;
		for (int i = 1; i < getWidth()-1; i++) {
			putAt(i, 0, Color.BLACK, Color.WHITE, 0xCD);
			putAt(i, getHeight()-1, Color.BLACK, Color.WHITE, 0xCD);
		}
		for (int i = 1; i < getHeight()-1; i++) {
			putAt(0, i, Color.BLACK, Color.WHITE, 0xBA);
			putAt(getWidth()-1, i, Color.BLACK, Color.WHITE, 0xBA);
		}
		putAt(0, 0, Color.BLACK, Color.WHITE, 0xC9);
		putAt(0, getHeight()-1, Color.BLACK, Color.WHITE, 0xC8);
		putAt(getWidth()-1, 0, Color.BLACK, Color.WHITE, 0xBB);
		putAt(getWidth()-1, getHeight()-1, Color.BLACK, Color.WHITE, 0xBC);
		for (int i = 1; i < getWidth()-1; i++) {
			putAt(i, 1, Color.BLACK, Color.WHITE, text[i-1]);
		}
	}
	@Override
	void onTouch(int x, int y) {
		onClick.run();
	}
}
