package com.example.adm.awa_ascii;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

public class AsciiView extends View implements View.OnTouchListener {
    private GameActivity act;
    private Bitmap bitmap;
    private Paint paint;
    private Sprite[] symbols;
    private Place[] places;
    private int w, h;
    int toX, toY;
    State state;

    private ArrayList<AsciiWindow> windows;

    private class Sprite {
        private Bitmap map;
        private Rect src;

        Sprite(Bitmap bitmap, Rect src) {
            this.map = bitmap;
            this.src = src;
        }

        void draw(Canvas canvas, Place place, Paint paint) {
            paint.setColor(place.bgColor);
            canvas.drawRect(place.getDst(), paint);
            paint.setColor(place.color);
            canvas.drawBitmap(map, src, place.getDst(), paint);
        }
    }
    private class Place {
        private Rect dst;
        private int ch;
        private int color;
        private int bgColor;

        Place(Rect dst) {
            this.dst = dst;
            this.bgColor = Color.BLACK;
        }
        Rect getDst() {
            return dst;
        }
        int getCh() {
            return ch;
        }
        void setCh(int ch) {
            if (ch >= 0 && ch < 256)
                this.ch = ch;
        }
        int getColor() {
            return color;
        }
        void setColor(int color) {
            this.color = color;
        }
        int getBg() {
            return bgColor;
        }
        void setBg(int color) {
            this.bgColor = color;
        }
    }
    private Bitmap getPic(int res, int n) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inDensity = 16;
        opt.inTargetDensity = n;
        Bitmap all = BitmapFactory.decodeResource(getResources(), res, opt);
        Bitmap ret2 = all.extractAlpha();
        all.recycle();
        return ret2;
    }
    public AsciiView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (isInEditMode()) return;
        windows = new ArrayList<>();

        act = (GameActivity) context;

        bitmap = getPic(R.drawable.ascii_x16, act.charSize);
        paint = new Paint();

        // sprites
        symbols = new Sprite[256];
        int xSize = bitmap.getWidth() / 16;
        int ySize = bitmap.getHeight() / 16;
        for (int sx = 0; sx < 16; sx++)
            for (int sy = 0; sy < 16; sy++) {
                int x = xSize * sx;
                int y = ySize * sy;
                symbols[sy * 16 + sx] = new Sprite(bitmap, new Rect(x, y, x + xSize, y + ySize));
            }
        setOnTouchListener(this);
    }
    Place getPlace(int x, int y) {
        if (x >= 0 && y >= 0 && x < w && y < h)
            return places[y * w + x];
        return null;
    }
    void putChar(int x, int y, int type) {
        Place place = getPlace(x, y);
        if (place != null && type >= 0 && type < 256)
            place.setCh(type);
        else
            Log.d(MenuActivity.LOG, "bad char!!!");
    }
    void putColor(int x, int y, int color) {
        Place place = getPlace(x, y);
        if (place != null)
            place.setColor(color);
    }
    void putBg(int x, int y, int color) {
        Place place = getPlace(x, y);
        if (place != null)
            place.setBg(color);
    }
    void init() {
        int xSize = bitmap.getWidth() / 16;
        int ySize = bitmap.getHeight() / 16;
        w = getWidth() / xSize;
        h = getHeight() / ySize;
        int xRel = (getWidth() - xSize*w)/2;
        int yRel = (getHeight() - ySize*h)/2;
        // create places
        places = new Place[w * h];
        for (int x = 0; x < w; x++)
            for (int y = 0; y < h; y++) {
                Place place = new Place(new Rect(
                        x * xSize + xRel,
                        y * ySize + yRel,
                        (x + 1) * xSize + xRel,
                        (y + 1) * ySize + yRel)
                );
                place.color = Color.WHITE;
                places[y * w + x] = place;
            }
        // create windows
        // main window. first three lines for header; last nine lines for buttons
        addWindow(new GameMapWindow(this, state, 0, 3, w, h-12));
        //addWindow(new ButtonWindow(this, 0, h-9, 5, 3, "kek"));
        addMovingButtons();
    }
    private void addMovingButtons() {
        char []up   = { 0x18 };
        char []dwn  = { 0x19 };
        char []lft  = { 0x1B };
        char []rgt  = { 0x1A };
        addWindow(new ButtonWindow(this, 3, h - 9, 3, 3, up, new Runnable() {
            @Override
            public void run() {
                buttonsMove(0, 1);
            }
        }));
        addWindow(new ButtonWindow(this, 0, h - 6, 3, 3, lft, new Runnable() {
            @Override
            public void run() {
                buttonsMove(-1, 0);
            }
        }));
        addWindow(new ButtonWindow(this, 3, h - 3, 3, 3, dwn, new Runnable() {
            @Override
            public void run() {
                buttonsMove(0, -1);
            }
        }));
        addWindow(new ButtonWindow(this, 6, h - 6, 3, 3, rgt, new Runnable() {
            @Override
            public void run() {
                buttonsMove(1, 0);
            }
        }));
        addWindow(new ButtonWindow(this, 3, h - 6, 3, 3, "0", new Runnable() {
            @Override
            public void run() {
                sendMove(toX, toY);
                toX = 0;
                toY = 0;
            }
        }));
    }
    public void addWindow(AsciiWindow win) {
        windows.add(win);
    }
    void sendMove(int dx, int dy) {
        GameActivity game = (GameActivity) getContext();
        if (game.isInWalkMode())
            game.getGameFragment().sendMove(dx, dy);
        else
            game.getGameFragment().sendReplace(dx, dy, (short)game.currentType);
    }
    void buttonsMove(int dx, int dy) {
        toX += dx;
        toY += dy;
        postInvalidate();
    }
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int spriteSize = bitmap.getWidth()/16;
        int disX = ((int) event.getX())/spriteSize;
        int disY = ((int) event.getY())/spriteSize;

        boolean found = false;
        for (AsciiWindow win : windows)
            if (win.isInside(disX, disY)) {
                win.onTouch(disX - win.getX(), disY - win.getY());
                found = true;
                break;
            }
        if (!found)
            Log.d("KEK", "Touch receiver not found");
        return false;
    }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (isInEditMode()) {
            canvas.drawColor(Color.rgb(0x00, 0xAA, 0x00));
            return;
        }
        // work with windows
        for (AsciiWindow win : windows) {
            win.update();
            win.draw();
        }
        // draw everything
        canvas.drawColor(Color.BLACK);
        for (int x = 0; x < w; x++)
            for (int y = 0; y < h; y++) {
                Place place = getPlace(x, y);
                if (place != null) {
                    Sprite symbol = symbols[place.getCh()];
                    symbol.draw(canvas, place, paint);
                }
            }
    }
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        init();
    }
}
