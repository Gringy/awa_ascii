package com.example.adm.awa_ascii;

import android.util.Log;

class AsciiWindow {
	private static class Char {
		int bg, fg, tp;
	}
	final private AsciiView view;
	final private int x, y, w, h;
	final private Char[] field;
	AsciiWindow(AsciiView view, int x, int y, int w, int h) {
		this.view = view;
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		field = new Char[w*h];
		for (int i = 0; i < w*h; i++)
			field[i] = new Char();
	}
	void putAt(int x, int y, int bg, int fg, int tp) {
		if (x < 0 || y < 0 || x >= w || y >= h)
			return;
		Char at = field[y*w+x];
		at.bg = bg;
		at.fg = fg;
		at.tp = tp;
	}
	void putBg(int x, int y, int bg) {
		if (x < 0 || y < 0 || x >= w || y >= h)
			return;
		Char at = field[y*w+x];
		at.bg = bg;
	}
	int getX() {
		return x;
	}
	int getY() {
		return y;
	}
	int getWidth() {
		return w;
	}
	int getHeight() {
		return h;
	}

	AsciiView getView() {
		return view;
	}

	void update() {}
	void draw() {
		Log.d("KEK", this.getClass().getCanonicalName()+" draws");
		for (int x = 0; x < w; x++)
			for (int y = 0; y < h; y++) {
				Char ch = field[y*w+x];
				view.putBg(this.x+x, this.y+y, ch.bg);
				view.putColor(this.x+x, this.y+y, ch.fg);
				view.putChar(this.x+x, this.y+y, ch.tp);
			}
	}
	boolean isInside(int x, int y) {
		return x >= this.x && y >= this.y && x < this.x+this.w && y < this.y+this.h;
	}
	void onTouch(int x, int y) {}
}
