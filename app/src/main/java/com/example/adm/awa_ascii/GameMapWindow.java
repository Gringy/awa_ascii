package com.example.adm.awa_ascii;

import android.graphics.Color;
import android.util.Log;

public class GameMapWindow extends AsciiWindow {
	final private State state;
	public GameMapWindow(AsciiView view, State state, int x, int y, int w, int h) {
		super(view, x, y, w, h);
		this.state = state;
	}
	private void drawWalls() {
		int w = getWidth();
		int h = getHeight();
		int midX = w/2;
		int midY = h/2;
		State.GameObj player = state.getPlayer();
		if (player == null) {
			Log.d("KEK", "GameMapWindow: drawWalls: player==null");
			return;
		}
		int playerChunkPos = state.getChunkPosition(player.gameID);
		for (int x = -midX-1; x < midX+1; x++)
			for (int y = -midY-1; y < midY+1; y++) {
				int worldX = player.x + x;
				int worldY = player.y + y;
				int tp = state.getType(playerChunkPos, worldX, worldY);
				State.GameType gtp = state.types.get(tp);
				if (gtp != null)
					putAt(midX+x, midY-y, gtp.bgColor, gtp.fgColor, gtp.sym);
				else
					putAt(midX+x, midY-y, Color.GREEN, Color.GREEN, 0);
			}
	}
	private int objToChar(int type) {
		switch (type) {
			case 0x01:
				return '@';
			case 0x02:
				return 'M';
			case 0x03:
				return 'F';
			default:
				return 'U';
		}
	}
	private int objToColor(int type) {
		switch (type) {
			case 0x01:
				return Color.GREEN;
			case 0x02:
				return Color.RED;
			case 0x03:
				return Color.YELLOW;
			default:
				return Color.BLUE;
		}
	}
	private void drawObjects() {
		int w = getWidth();
		int h = getHeight();
		int midX = w/2;
		int midY = h/2;
		State.GameObj player = state.getPlayer();
		if (player == null) return;
		State.Chunk[] chunks = state.getChunks();
		int playerChPos = state.getChunkPosition(player.gameID);
		int px = (playerChPos % 3 - 1)*32;
		int py = (playerChPos / 3 - 1)*32;
		for (int i = 0; i < 9; i++) if (chunks[i] != null) {
			State.Chunk chunk = chunks[i];
			int chunkX = i % 3 - 1;
			int chunkY = i / 3 - 1;
			for (State.GameObj obj : chunk.objects) {
				int drawX = midX + chunkX*32 - px + obj.x - player.x;
				int drawY = midY - chunkY*32 + py - obj.y + player.y;
				putAt(drawX, drawY, 0xFF200000, objToColor(obj.type), objToChar(obj.type));
			}
		}
	}
	private void drawWave() {
		int w = getWidth();
		int h = getHeight();
		int midX = w/2;
		int midY = h/2;
		State.GameObj player = state.getPlayer();
		if (player == null) return;
		Waver waver = state.playerWaver;
		for (int x = -5; x <= 5; x++)
			for (int y = -5; y <= 5; y++) {
				int type = waver.getAt(x, y);
				if (type != Waver.WALL && type != Waver.UNCHECKED)
					putBg(midX+x, midY-y, Color.rgb(0x33, 0x77, 0x33));
			}
		if (getView().toX != 0 || getView().toY != 0) {
			putAt(midX+getView().toX, midY-getView().toY, Color.rgb(0xaa, 0x55, 0x55), Color.rgb(0xFF, 0, 0), 'X');
		}
	}
	@Override
	void update() {
		Log.d("KEK", "GameMapWindow update");
		synchronized (state) {
			drawWalls();
			drawObjects();
			drawWave();
		}
	}
	@Override
	void onTouch(int x, int y) {
		getView().sendMove(x - getWidth()/2, getHeight()/2 - y);
		Log.d("KEK", "touched to: " + (x-getWidth()/2) + "  " + (getHeight()/2 - y));
	}
}
