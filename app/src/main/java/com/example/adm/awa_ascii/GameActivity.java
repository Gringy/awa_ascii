package com.example.adm.awa_ascii;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;

public class GameActivity extends Activity implements View.OnClickListener {
    private AsciiView gameView;
    int currentType = 0;

    private String iLogin, iPass, iServer;
    int charSize;

    private TextView typeText;
    private ToggleButton modeToggle;
    private EditText sayText;
    private GameFragment gameFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        iLogin = extras.getString(MenuActivity.ARG_LOGIN);
        iPass = extras.getString(MenuActivity.ARG_PASSWD);
        iServer = extras.getString(MenuActivity.ARG_SERV);
        charSize = extras.getInt(MenuActivity.ARG_SIZE);

        setContentView(R.layout.activity_game);

        gameView = (AsciiView) findViewById(R.id.gameView);

        sayText = (EditText) findViewById(R.id.sayEditText);

        typeText = (TextView) findViewById(R.id.typeNameText);
        Button sayButton = (Button) findViewById(R.id.sayButton);
        sayButton.setOnClickListener(this);
        Button leftButton = (Button) findViewById(R.id.leftTypeButton);
        leftButton.setOnClickListener(this);
        Button rightButton = (Button) findViewById(R.id.rightTypeButton);
        rightButton.setOnClickListener(this);

        modeToggle = (ToggleButton) findViewById(R.id.editModeToggle);
        modeToggle.setOnClickListener(this);

        gameFragment = GameFragment.newInstance(this, iLogin, iPass, iServer);
        getFragmentManager().beginTransaction().add(gameFragment, "GAME_FRAGMENT").commit();
        gameView.state = gameFragment.getState();
    }

    void updateTypeText(int dt) {
        currentType += dt;
        ArrayList<State.GameType> types = gameFragment.getState().sortedTypes;
        if (currentType < 0)
            currentType = types.size()-1;
        if (currentType >= types.size())
            currentType = 0;
        State.GameType type = types.get(currentType);
	    String text = "#" + type.id + ": ";
	    for (String name : type.names)
	    	text += name + " ";
        typeText.setText(text);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sayButton: {
                gameFragment.sendSay(sayText.getText().toString());
                break;
            }
            case R.id.rightTypeButton:
                updateTypeText(1);
                break;
            case R.id.leftTypeButton:
                updateTypeText(-1);
                break;
            case R.id.editModeToggle:
                break;
        }
    }
    AsciiView getGameView() {
        return gameView;
    }
    GameFragment getGameFragment() {
        return gameFragment;
    }
    boolean isInWalkMode() {
        return !modeToggle.isChecked();
    }
}
