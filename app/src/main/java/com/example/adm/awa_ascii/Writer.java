package com.example.adm.awa_ascii;

import java.nio.charset.Charset;
import java.util.ArrayList;

class Writer {
    private ArrayList<Byte> bytes;

    Writer() {
        bytes = new ArrayList<>();
    }

    void writeByte(byte bt) {
        bytes.add(bt);
    }

    void writeShort(short sht) {
        bytes.add((byte) (sht >>> 8));
        bytes.add((byte) (sht & 0xFF));
    }

    void writeInt(int nt) {
        bytes.add((byte) (nt >>> 24));
        bytes.add((byte) (nt >>> 16 & 0xFF));
        bytes.add((byte) (nt >>> 8 & 0xFF));
        bytes.add((byte) (nt & 0xFF));
    }

    void writeString(String str) {
        byte[] bytes = str.getBytes(Charset.forName("utf-8"));
        writeShort((short) bytes.length);
        for (int i = 0; i < bytes.length; i++)
            writeByte(bytes[i]);
    }

    byte[] getBytes() {
        byte[] ret = new byte[bytes.size()];
        for (int i = 0; i < bytes.size(); i++) {
            ret[i] = bytes.get(i);
        }
        return ret;
    }
}
