package com.example.adm.awa_ascii;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

class State {
    private static final int CHUNK_NUM = 9;
    private static final int CHUNK_DIM = 32;

    // top level messages
    private final byte MSG_INIT_TYPE = 0x00;
    private final byte MSG_DHARMA_TYPE = 0x01;
    private final byte MSG_CHUNKS_TYPE = 0x02;

    // dharmas
    private final byte MSG_MOVE_TYPE = 0x00;
    private final byte MSG_SAY_TYPE = 0x01;
    private final byte MSG_ADD_TYPE = 0x02;
    private final byte MSG_DEL_TYPE = 0x03;
    private final byte MSG_CHANGE_TYPE = 0x04;

    private Chunk[] chunks;
    private int playerID;
    private boolean playerChanges;

    Waver playerWaver;
	HashMap<Integer, GameType> types;
	ArrayList<GameType> sortedTypes;

    static class Chunk {
        Chunk() {
            map = new int[32*32];
            objects = new LinkedList<>();
        }
        int chunkID;
        int[] map;
        LinkedList<GameObj> objects;
    }
    static class GameObj {
        int gameID;
        int x, y;
        short type;
    }
    static class GameType {
	    int id;
	    int sym;
	    int bgColor, fgColor;
	    String[] names;
	    boolean pass;
    }
    // fields must be public for gson
    public static class i {
        public int id;
        public String[] names;
        public boolean pass;
        public String bg, fg;
        public String sym;
    }
    State() {
        chunks = new Chunk[CHUNK_DIM];
        playerWaver = new Waver();
	    types = new HashMap<>();
	    sortedTypes = new ArrayList<>();
    }
    void InitTypes(Context ctx) {
        try {
            Gson gson = new Gson();
            InputStream typesJson = ctx.getResources().openRawResource(R.raw.cltypes);
            InputStreamReader reader = new InputStreamReader(typesJson);
            i[] tps = gson.fromJson(reader, i[].class);
            for (i record : tps) if (record != null) {
                    GameType tp = new GameType();
                    tp.id = record.id;
                    tp.names = record.names;
                    tp.pass = record.pass;
                    tp.sym = record.sym.charAt(0);
                    tp.bgColor = Color.parseColor("#"+record.bg);
                    tp.fgColor = Color.parseColor("#"+record.fg);
                    types.put(record.id, tp);
	                sortedTypes.add(tp);
                }
        } catch (Exception e) {
            Log.d(MenuActivity.LOG, "Error on parse types json file", e);
        }
    }
    boolean isPassableType(int type) {
	    GameType tp = types.get(type);
        return tp != null ? tp.pass : false;
    }
    Chunk[] getChunks() {
        return chunks;
    }
    @Nullable
    private GameObj getObj(int id) {
        for (Chunk chunk : chunks) if (chunk != null) {
            for (GameObj obj : chunk.objects) {
                if (obj.gameID == id)
                    return obj;
            }
        }
        return null;
    }
    int getType(int chunkPos, int x, int y) {
        x += (chunkPos % 3 - 1)*CHUNK_DIM;
        y += (chunkPos / 3 - 1)*CHUNK_DIM;
        int chX = 0, chY = 0;
        if (x < 0) {
            chX = x / CHUNK_DIM - 1;
            x = CHUNK_DIM + x % CHUNK_DIM;
        }
        if (y < 0) {
            chY = y / CHUNK_DIM - 1;
            y = CHUNK_DIM + y % CHUNK_DIM;
        }
        if (x >= CHUNK_DIM) {
            chX = x / CHUNK_DIM;
            x = x % CHUNK_DIM;
        }
        if (y >= CHUNK_DIM) {
            chY = y / CHUNK_DIM;
            y = y % CHUNK_DIM;
        }
        if (chX > 1 || chX < -1 || chY > 1 || chY < -1)
            return 1;
        Chunk chunk = chunks[(chY+1)*3 + chX + 1];
        if (chunk == null)
            return 2;
        return chunk.map[y*CHUNK_DIM+x];
    }
    int getChunkPosition(int gameID) {
        for (int i = 0; i < 9; i++) if (chunks[i] != null)
            for (GameObj obj : chunks[i].objects)
                if (obj.gameID == gameID)
                    return i;
        return 0;
    }
    @Nullable
    GameObj getPlayer() {
        return getObj(playerID);
    }
    void Message(byte[] data) {
        Reader reader = new Reader(data);
        try {
            byte type = reader.readByte();
            switch (type) {
                // init message
                case MSG_INIT_TYPE:
                    parseInit(reader);
                    break;
                // dharma message
                case MSG_DHARMA_TYPE:
                    parseDharma(reader);
                    break;
                // change message
                case MSG_CHUNKS_TYPE:
                    parseChange(reader);
                    break;
                default:
                    // unknown message
            }
        } catch (Exception e) {
            Log.d(MenuActivity.LOG, "exception on parse message: " + e.getMessage());
        }
        if (playerChanges) {
            updatePath();
            playerChanges = false;
        }
    }
    private void parseChange(Reader reader) throws Exception {
        int[] cids = new int[CHUNK_NUM];
        Chunk[] oldChunks = chunks;
        Chunk[] newChunks = new Chunk[9];
        for (int i = 0; i < CHUNK_NUM; i++)
            cids[i] = reader.readInt();
        int unknown = 0;
        for (int i = 0; i < CHUNK_NUM; i++) {
            boolean found = false;
            for (int j = 0; j < CHUNK_NUM; j++)
                if (oldChunks[j] != null && oldChunks[j].chunkID == cids[i]) {
                    newChunks[i] = oldChunks[j];
                    found = true;
                    break;
                }
            if (!found)
                unknown++;
        }
        for (int i = 0; i < unknown; i++) {
            Chunk chunk = parseChunk(reader);
            if (chunk != null) {
                for (int j = 0; j < CHUNK_NUM; j++) if (cids[j] == chunk.chunkID)
                    newChunks[j] = chunk;
            }
        }
        chunks = newChunks;
        playerChanges = true;
    }
    private void parseInit(Reader reader) throws Exception {
        playerID = reader.readInt();
        for (int i = 0; i < CHUNK_NUM; i++) {
            chunks[i] = parseChunk(reader);
        }
        playerChanges = true;
    }
    private void parseDharma(Reader reader) throws Exception {
        short numOfDharmas = reader.readShort();
        for (int i = 0; i < numOfDharmas; i++) {
            doDharma(reader);
        }
    }
    private void doDharma(Reader reader) throws Exception {
        byte type = reader.readByte();
        switch (type) {
            // move dharma
            case MSG_MOVE_TYPE: {
                int gameID = reader.readInt();
                byte x = reader.readByte();
                byte y = reader.readByte();
                GameObj obj = getObj(gameID);
                if (obj != null) {
                    obj.x = x;
                    obj.y = y;
                }
                if (gameID == playerID) playerChanges = true;
                break;
            }
            // say dharma
            case MSG_SAY_TYPE: {
                int gameID = reader.readInt();
                String msg = reader.readString();
                Log.d(MenuActivity.LOG, "#" + gameID + " says: " + msg);
                break;
            }
            // add dharma
            case MSG_ADD_TYPE: {
                GameObj obj = new GameObj();
                int chunkID = reader.readInt();
                obj.gameID = reader.readInt();
                obj.type = reader.readShort();
                obj.x = reader.readByte();
                obj.y = reader.readByte();
                for (int i = 0; i < CHUNK_NUM; i++) if (chunks[i] != null)
                    if (chunks[i].chunkID == chunkID)
                        chunks[i].objects.add(obj);
                if (obj.gameID == playerID) playerChanges = true;
                break;
            }
            // del dharma
            case MSG_DEL_TYPE: {
                int chunkID = reader.readInt();
                int gameID = reader.readInt();
                for (int i = 0; i < CHUNK_NUM; i++) if (chunks[i] != null)
                    if (chunks[i].chunkID == chunkID)
                        for (int j = 0; j < chunks[i].objects.size(); j++)
                            if (chunks[i].objects.get(j).gameID == gameID) {
                                chunks[i].objects.remove(j);
                                break;
                            }
                if (gameID == playerID) playerChanges = true;
                break;
            }
            // change map dharma
            case MSG_CHANGE_TYPE: {
                int chunkID = reader.readInt();
                int x = reader.readByte();
                int y = reader.readByte();
                short tp = reader.readShort();
                for (Chunk chunk : chunks) if (chunk.chunkID == chunkID) {
                    chunk.map[y*CHUNK_DIM+x] = tp;
                }
                playerChanges = true;
                break;
            }
        }
    }
    private void updatePath() {
        GameObj player = getPlayer();
        if (player == null) return;
        playerWaver.init(this, player.x, player.y);
        playerWaver.move(5);
    }
    void Clear() {
        for (int i = 0; i < chunks.length; i++)
            chunks[i] = null;
        playerID = 0;
    }
    @Nullable
    private Chunk parseChunk(Reader reader) throws Exception {
        int chunkID = reader.readInt();
        if (chunkID == -1)
            return null;
        Chunk chunk = new Chunk();
        chunk.chunkID = chunkID;
        for (int i = 0; i < CHUNK_DIM*CHUNK_DIM; i++)
            chunk.map[i] = reader.readShort();
        int numOfObjects = reader.readShort();
        for (int i = 0; i < numOfObjects; i++) {
            GameObj obj = new GameObj();
            obj.gameID = reader.readInt();
            obj.type = reader.readShort();
            obj.x = reader.readByte();
            obj.y = reader.readByte();
            chunk.objects.add(obj);
        }
        Log.d(MenuActivity.LOG, "Parsed chunk #" + chunkID);
        return chunk;
    }
}
