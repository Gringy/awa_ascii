package com.example.adm.awa_ascii;

import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;

import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFactory;
import com.neovisionaries.ws.client.WebSocketFrame;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class GameFragment extends Fragment {
	private String mLogin;
	private String mPassword;
	private String mServer;

	private final State state = new State();
	private WebSocket ws;
	private GameActivity act;

	public GameFragment() {
		// Required empty public constructor
	}

	public static GameFragment newInstance(GameActivity act, String login, String pwd, String server) {
		GameFragment fragment = new GameFragment();
		Bundle args = new Bundle();
		args.putString(MenuActivity.ARG_LOGIN, login);
		args.putString(MenuActivity.ARG_PASSWD, pwd);
		args.putString(MenuActivity.ARG_SERV, server);
		fragment.setArguments(args);
		fragment.act = act;
		//fragment.setRetainInstance(true);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			mLogin = getArguments().getString(MenuActivity.ARG_LOGIN);
			mPassword = getArguments().getString(MenuActivity.ARG_PASSWD);
			mServer = getArguments().getString(MenuActivity.ARG_SERV);
		}
		state.InitTypes(act);
	}

	@Override
	public void onStart() {
		super.onStart();
		WebSocketFactory factory = new WebSocketFactory();
		try {
			ws = factory.createSocket("ws://" + mServer + "/gamesock/", 10000);
		} catch (IOException e) {
			return;
		}
		ws.connectAsynchronously();
		ws.addListener(new WebSocketAdapter() {

			@Override
			public void onConnected(WebSocket websocket, Map<String, List<String>> headers) throws Exception {
				Log.d(MenuActivity.LOG, "Connected!!");
				sendConnect();
			}
			@Override
			public void onBinaryMessage(WebSocket websocket, byte[] binary) throws Exception {
				synchronized (state) {
					state.Message(binary);
				}
				act.getGameView().postInvalidate();
			}
			@Override
			public void onError(WebSocket websocket, WebSocketException cause) throws Exception {
				Log.d(MenuActivity.LOG, "onError" + cause.getMessage());
				synchronized (state) {
					state.Clear();
				}
				act.getGameView().postInvalidate();
			}
			@Override
			public void onConnectError(WebSocket websocket, WebSocketException exception) throws Exception {
				Log.d(MenuActivity.LOG, "onConnectError");
			}
			@Override
			public void onDisconnected(WebSocket websocket, WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame, boolean closedByServer) throws Exception {
				Log.d(MenuActivity.LOG, "onDisconnected");
			}
		});
	}
	@Override
	public void onStop() {
		super.onStop();
		if (ws != null) {
			ws.disconnect();
			ws = null;
		}
	}
	State getState() {
		return state;
	}
	// Messages
	final byte MSG_SAY_TYPE = 0x00;
	final byte MSG_MOVE_TYPE = 0x01;
	final byte MSG_REPLACE_TYPE = 0x02;
	private void sendConnect() {
		if (ws != null) {
			Writer writer = new Writer();
			writer.writeString(mLogin);
			writer.writeString(mPassword);
			ws.sendBinary(writer.getBytes());
		}
	}
	void sendSay(String msg) {
		if (ws != null) {
			Writer writer = new Writer();
			writer.writeByte(MSG_SAY_TYPE);
			writer.writeString(msg);
			ws.sendBinary(writer.getBytes());
		}
	}
	void sendMove(int dx, int dy) {
		if (ws != null) {
			int type = state.playerWaver.getAt(dx, dy);
			if (type != Waver.UNCHECKED && type != Waver.WALL) {
				Writer writer = new Writer();
				writer.writeByte(MSG_MOVE_TYPE);
				writer.writeByte((byte) dx);
				writer.writeByte((byte) dy);
				ws.sendBinary(writer.getBytes());
			}
		}
	}
	void sendReplace(int dx, int dy, short type) {
		if (ws != null) {
			Writer writer = new Writer();
			writer.writeByte(MSG_REPLACE_TYPE);
			writer.writeByte((byte) dx);
			writer.writeByte((byte) dy);
			writer.writeShort(type);
			ws.sendBinary(writer.getBytes());
		}
	}
}
