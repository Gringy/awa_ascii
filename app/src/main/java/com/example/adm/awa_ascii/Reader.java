package com.example.adm.awa_ascii;

class Reader {
    private byte[] data;
    private int pointer;
    Reader(byte[] data) {
        this.data = data;
        this.pointer = 0;
    }
    private int count() {
        return this.data.length - this.pointer;
    }
    byte readByte() throws Exception {
        if (count() < 1)
            throw new Exception("cannot read byte");
        return data[pointer++];
    }
    short readShort() throws Exception {
        if (count() < 2)
            throw new Exception("cannot read short");
        short ret = (short)(readByte() << 8);
        ret = (short)(ret | readByte());
        return ret;
    }
    int readInt() throws Exception {
        if (count() < 2)
            throw new Exception("cannot read int");
        int ret = readByte() << 24;
        ret = ret | (readByte() << 16);
        ret = ret | (readByte() << 8);
        ret = ret | readByte();
        return ret;
    }
    String readString() throws Exception {
        short length = readShort();
        if (count() < length)
            throw new Exception("too few bytes to decode string");
        byte[] bytes = new byte[length];
        System.arraycopy(data, pointer, bytes, 0, length);
        return new String(bytes, "utf-8");
    }
}
