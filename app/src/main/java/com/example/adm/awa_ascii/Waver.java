package com.example.adm.awa_ascii;

class Waver {
	static final int UNCHECKED = -1;
	static final int WALL = -2;

	private static final int WALKING_DISTANCE = 5;
	private static final int FIELD_SIZE = WALKING_DISTANCE*2 + 1;

	private int[] field;
	private int current;
	Waver() {
		field = new int[FIELD_SIZE*FIELD_SIZE];
		// -5..5
	}
	void init(State state, int objX, int objY) {
		for (int x = -WALKING_DISTANCE; x <= WALKING_DISTANCE; x++)
			for (int y = -WALKING_DISTANCE; y <= WALKING_DISTANCE; y++) {
				int type = state.getType(4, objX+x, objY+y);
				if (state.isPassableType(type))
					setAt(x, y, UNCHECKED);
				else
					setAt(x, y, WALL);
			}
		setAt(0, 0, 0);
		current = 0;
	}
	private void setAt(int x, int y, int value) {
		x += 5; y += 5;
		if (x < 0 || y < 0 || x >= 11 || y >= 11)
			return;
		field[y*11+x] = value;
	}
	int getAt(int x, int y) {
		x += 5; y += 5;
		if (x < 0 || y < 0 || x >= 11 || y >= 11)
			return WALL;
		return field[y*11+x];
	}
	private void move() {
		for (int x = -5; x <= 5; x++)
			for (int y = -5; y <= 5; y++) {
				int type = getAt(x, y);
				if (type == current) {
					if (getAt(x, y+1) == UNCHECKED)
						setAt(x, y+1, current+1);
					if (getAt(x, y-1) == UNCHECKED)
						setAt(x, y-1, current+1);
					if (getAt(x+1, y) == UNCHECKED)
						setAt(x+1, y, current+1);
					if (getAt(x-1, y) == UNCHECKED)
						setAt(x-1, y, current+1);
				}
			}
		current++;
	}
	void move(int times) {
		for (int i = 0; i < times; i++)
			move();
	}
}
