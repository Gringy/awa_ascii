package com.example.adm.awa_ascii;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;

public class MenuActivity extends Activity implements View.OnClickListener {
    static final String ARG_LOGIN = "login";
    static final String ARG_PASSWD = "pwd";
    static final String ARG_SERV = "serv";
    static final String ARG_SIZE = "size";

    static final String LOG = "Awa";

    private EditText serv, login, pass;
    private SeekBar sizeBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Button playButton = (Button) findViewById(R.id.playButton);
        Button helpButton = (Button) findViewById(R.id.helpButton);
        serv = (EditText) findViewById(R.id.serverText);
        login = (EditText) findViewById(R.id.loginText);
        pass = (EditText) findViewById(R.id.passwordText);

        sizeBar = (SeekBar) findViewById(R.id.sizeBar);

        playButton.setOnClickListener(this);
        helpButton.setOnClickListener(this);

        // load settings
        SharedPreferences prefs = getPreferences(MODE_PRIVATE);

        String pServ = prefs.getString(ARG_SERV, getString(R.string.defaultServer));
        serv.setText(pServ);
        String pLogin = prefs.getString(ARG_LOGIN, getString(R.string.defaultLogin));
        login.setText(pLogin);
        String pPass = prefs.getString(ARG_PASSWD, getString(R.string.defaultPassword));
        pass.setText(pPass);
        int pSize = prefs.getInt(ARG_SIZE, 0);
        sizeBar.setProgress(pSize);
    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(ARG_LOGIN, login.getText().toString());
        editor.putString(ARG_SERV, serv.getText().toString());
        editor.putString(ARG_PASSWD, pass.getText().toString());
        editor.putInt(ARG_SIZE, sizeBar.getProgress());
        editor.apply();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.playButton: {
                Intent intent = new Intent(this, GameActivity.class);
                intent.putExtra(ARG_SERV, serv.getText().toString());
                intent.putExtra(ARG_LOGIN, login.getText().toString());
                intent.putExtra(ARG_PASSWD, pass.getText().toString());
                intent.putExtra(ARG_SIZE, sizeBar.getProgress()+16);
                startActivity(intent);
                break;
            }
            case R.id.helpButton: {
                Intent intent = new Intent(this, HelpActivity.class);
                startActivity(intent);
                break;
            }
        }
    }
}
